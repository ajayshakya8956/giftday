<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\v1\AuthController;
use App\Http\Controllers\Api\v1\ProjectController;
use App\Http\Controllers\Api\v1\RoleController;
use App\Http\Controllers\Api\v1\Category\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Route::post('register', [AuthController::class, 'register']);
//Route::post('login', [AuthController::class, 'login']);


Route::group(['prefix' => 'v1', 'middleware' => 'guest'], function () {

    /* user login routes */
	Route::group(['prefix' => 'user'], function () {
	    Route::post('register', [AuthController::class, 'register']);
	    Route::post('login',  [AuthController::class, 'login']);
	   
	});

});

Route::group(['prefix' => 'v1'], function () {

	Route::group(['prefix' => 'role','middleware'=>'auth:api'], function () {

	    Route::post('create', [RoleController::class, 'store']);   
	   
	});

	Route::group(['prefix' => 'category','middleware'=>'guest'], function () {

	    Route::post('create', [CategoryController::class, 'create']);   
	   
	});

});