<?php

namespace App\Http\Controllers\Api\Auth\v1;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Validator;
use Mail;

class AuthController extends Controller
{
    public function register(Request $request)
    {

    	$validator = Validator::make($request->all(), [ 
        'name' => 'required', 
        'email' => 'required|email|unique:users', 
        'password' => 'required', 
        'confirm_password' => 'required|same:password',
         
      ]);

  	  if ($validator->fails()) { 
  	   return response()->json(['error'=>$validator->errors()], 401);            
  	  }

      $input = $request->all(); 
    	$input['password'] = bcrypt($input['password']); 

      $user = User::create($input); 

      $user->assignRole($request->input('roles'));
     
    	$accessToken = $user->createToken('authToken')->accessToken;
       
      	return response()->json(['success'=>true,"data"=>$user,"token"=>$accessToken], 200); 

        
    }

    public function login(Request $request)
    {
       
        if(Auth::guard('web')->attempt(['email' => request('email'), 'password' => request('password')])){ 
           
            $user = Auth::user(); 
           
          	$accessToken = $user->createToken('authToken')->accessToken;
       
      		return response()->json(['success'=>true,"data"=>$user,"token"=>$accessToken], 200); 
            
        } else { 
        	
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 

    }
}