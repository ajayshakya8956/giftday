<?php

namespace App\Http\Controllers\Api\V1\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function create(Request $request)
    {
    	//$disk = Storage::disk('public');

   		$cat = Category::create([
   			'name'=>$request->name,
   			'isActive'=>'true',
   		]);
   		$cat->save();
    	if ($request->hasFile('image')) {

    		$request->validate([
            	'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	]);

			//$filenameWithExt = $request->file('image')->getClientOriginalName ();
			// Get Filename
			//$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
			// Get just Extension
			//$extension = $request->file('image')->getClientOriginalExtension();
			// Filename To store
			//$fileNameToStore = $filename. '_'. time().'.'.$extension;

			$imageName = "cat_".time().'.'.$request->image->extension();  

			$request->image->move(public_path('Category'), $imageName);

			//$request->request->add(['image' =>$imageName]);
			Category::where(['id'=>$cat->id])
			->update(['image'=>$imageName]);
			$cat->image = $imageName;
			// Upload Image$path = $request->file(‘image’)->storeAs(‘public/image’, $fileNameToStore);
		} 
    	
    	return response()->json(['success'=>true,"data"=>$cat], 200); 
     
        
    	
    }
}
