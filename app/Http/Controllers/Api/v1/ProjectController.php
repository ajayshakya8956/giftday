<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Str;

class ProjectController extends Controller
{
    
    public function detail()
    {
    	$data['login'] = "success";
    	return response()->json(['success'=>true,"data"=>$data], 200); 
    }
}
