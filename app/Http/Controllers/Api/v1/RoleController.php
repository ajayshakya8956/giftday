<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use DB;

class RoleController extends Controller
{

    function __construct()
    {
        //$this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:role-create', ['only' => ['create','store']]);
        // $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        // $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }
    
    // public function index(Request $request)
    // {
    //     $roles = Role::orderBy('id','DESC')->paginate(5);
    //     return view('roles.index',compact('roles'));
    // }
    
    // public function create()
    // {
    //     $permission = Permission::get();
    //     return view('roles.create',compact('permission'));
    // }
     
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required|unique:roles,name',
        //     'permission' => 'required',
        // ]);
    	//print_r($request->all());
    	
        Validator::make($request->all(), [ 
	        'name' => 'required|unique:roles,name',
	        'permission' => 'required',
	         
      	]);
    
        //$role = Role::create(['name' => $request->input('role')]);
       	$role = Role::create(['name' => 'partner']);
      	$role->givePermissionTo('product-create','product-list','product-edit','product-delete','category-list');

        //$role->syncPermissions($request->input('permission'));
       
		//$permission = Permission::create(['name' => 'edit articles']);
        return response()->json(['success'=>true,"data"=>$role], 200); 
    	
        //return redirect()->route('roles.index')
        //->with('success','Role created successfully');
    }

    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();
    
        //return view('roles.show',compact('role','rolePermissions'));
    }
    
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
    
        return view('roles.edit',compact('role','permission','rolePermissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);
    
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
    
        $role->syncPermissions($request->input('permission'));
    
        return redirect()->route('roles.index')
                        ->with('success','Role updated successfully');
    }

    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('roles.index')
                        ->with('success','Role deleted successfully');
    }
}